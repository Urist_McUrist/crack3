#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings


// Stucture to hold both a plaintext password and a hash.
struct entry 
{
   char *password;
   char *hash;
};

//string compare for qsort function
int stringCmp(const void *a, const void *b){
    struct entry *aa = (struct entry*)a;
    struct entry *bb = (struct entry*)b;
    return strcmp(aa->hash, bb->hash);
}

//read in the password dictionary and create array of structures with
//the password and matching hash
struct entry *read_dictionary(char *filename, int *size)
{
    //create the stat struct and stat the file to get the filesize
    struct stat info;
    if ((stat(filename, &info)) == -1){
        printf("Can't stat the file\n");
        exit(1);
    }
    
    //set the filesize and malloc space for the whole file.
    int filesize = info.st_size;
    char *contents = malloc(filesize + 1);
    
    //open the dictionary for reading
    FILE * dictionary = fopen(filename, "r");
    if(!dictionary){
        printf("Can't open dictionary file for reading");
        exit(1);
    }
    
    //read the eentire file into memory and close the file
    fread(contents, 1, filesize, dictionary);
    fclose(dictionary);
    contents[filesize] = '\0';
    
    //find the amount of lines within the file to determine the size of the
    //struct array
    int nlcount = 0;
    for(int i = 0; i < filesize; i++){
        if(contents[i] == '\n'){
            nlcount++;
        }
    }
    
    //create the struct array
    struct entry * entryArray = malloc(nlcount * sizeof(struct entry));
    
    //use the first string tokenizer outside of the loop to set contents as the stream
    char *line = strtok(contents, "\n"); //
    entryArray[0].password = line;
    entryArray[0].hash = md5(line, strlen(line));
    
    int i = 1;
    while((line = strtok(NULL, "\n")) != NULL){
        entryArray[i].password = line; //set the password in the struct
        entryArray[i].hash = md5(line, strlen(line)); //hash the password for the struct
        i++;
    }
    
    *size = nlcount;
    free(contents);
    return entryArray;
}


int main(int argc, char *argv[])
{
    //check if program parameters are set up correctly
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    
    //use read_dictionary to create the dictionary and set it's size
    int dSize;
    struct entry *dict = read_dictionary(argv[2], &dSize);
    
    //sort the dictionary
    qsort(dict, dSize, sizeof(struct entry), stringCmp);
    
    //open the hash file for reading
    FILE * hashes = fopen(argv[1], "r");
    if(!hashes){
        printf("Can't open \"%s\" for reading", argv[1]);
        exit(1);
    }
    
    
    char hash[33]; //char array to store each hash
    int hashNumber = 1;
    //use a while loop and fread to grab each hash from the file
    while(fread(hash, sizeof(char), 33, hashes)){
        hash[strlen(hash)-1] = '\0'; //fread grabs the \n so replace with null
        
        //loop through the struct array and compare the hash to each of the
        //pre-computed hashes
        for(int i = 0; i < dSize; i++){ 
            if(strcmp(hash, dict[i].hash) == 0){
                printf("Found Hash #%d, %s == %s\n", hashNumber, hash, dict[i].password);
                continue;
            }
        }
        
        hashNumber++;
    }
    
    exit(0);
}